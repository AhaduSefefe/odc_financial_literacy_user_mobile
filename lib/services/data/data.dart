import '../../environment/globals.dart' as globals;

import 'dart:convert' as convert;
import 'package:http/http.dart' as http;

class Data {
  var url;
  Data() {
    this.url = globals.url;
  }

  Future<Object> getData(var port) async {
    final http.Response response = await http.get(Uri.https(this.url, port));
    if (response.statusCode == 200) {
      print("got data");
      // return convert.jsonDecode(response.body);
    } else {
      throw Exception("Can't get data. Error of type " +
          response.statusCode.toString() +
          " happened");
    }
  }

  Future<Object> createData(String port, Object data) async {
    final http.Response response = await http.post(
      Uri.https(this.url, port),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: convert.jsonEncode(data),
    );
    if (response.statusCode == 200) {
      print("created data");
      // return convert.jsonDecode(response.body);
    } else {
      throw Exception("Can't create data. Error of type " +
          response.statusCode.toString() +
          " happened");
    }
  }

  Future<Object> updateData(String port, Object data) async {
    final http.Response response = await http.put(
      Uri.https(this.url, port),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: convert.jsonEncode(data),
    );
    if (response.statusCode == 200) {
      print("updated data");
      // return convert.jsonDecode(response.body);
    } else {
      throw Exception("Can't update data. Error of type " +
          response.statusCode.toString() +
          " happened");
    }
  }

  Future<String> deleteData(
    String port,
    var id,
  ) async {
    final http.Response response = await http.delete(
      Uri.https(this.url, port + "/" + id.toString()),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
    );
    if (response.statusCode == 200) {
      print("deleted data");
      // return convert.jsonDecode(response.body);
    } else {
      throw Exception("Can't delete data. Error of type " +
          response.statusCode.toString() +
          " happened");
    }
  }
}
